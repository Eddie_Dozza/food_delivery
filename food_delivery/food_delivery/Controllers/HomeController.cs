﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using food_delivery.Data;
using Microsoft.AspNetCore.Mvc;
using food_delivery.Models;
using food_delivery.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace food_delivery.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
           var companies =  _context.Companies.ToList();
            return View(companies);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Company company)
        {
            _context.Companies.Add(company);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult CreateDish()
        {
            
            List<Company> companies = _context.Companies.ToList();
            ViewBag.Companies = new SelectList(companies, "Id", "Name");
            
            return View();
        }

        [HttpPost]
        public IActionResult CreateDish(Dish dish)
        {
            _context.Dishes.Add(dish);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Companies.Include(p => p.Dishes)
                .SingleOrDefaultAsync(m => m.Id == id);
            //List<Dish> dishes = _context.Dishes.ToList();
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        public async Task<IActionResult> Cart(int id)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

        CartViewModel model = new CartViewModel()
            {
                User = user,
                Dishes = _context.Dishes.Where(d => d.Company.Id == id).ToList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(int id)
        {
            //Company company = _context.Companies.Find(id);
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
        
                Cart cart = new Cart()
                {
                    UserId = user.Id,
                    DishId = id
                    //Quantity = +1

                };

                _context.Carts.Add(cart);
                _context.SaveChanges();

        return RedirectToAction("Index");
        }
    }
}
