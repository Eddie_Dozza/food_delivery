﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food_delivery.Models
{
    public class Company
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public List<Dish> Dishes { get; set; } 

        public Company()
        {
            Dishes = new List<Dish>();  
        }
    }
}
