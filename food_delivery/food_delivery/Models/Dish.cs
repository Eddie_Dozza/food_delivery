﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace food_delivery.Models
{
    public class Dish
    {
        public int Id { get; set; }

        public string Name { get; set; }    

        public string Description { get; set; }
        public int Price { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
        public string ApplicationUserId { get; set; }  

        public Company Company { get; set; }    
        public int CompanyId { get; set; }
    }
}
