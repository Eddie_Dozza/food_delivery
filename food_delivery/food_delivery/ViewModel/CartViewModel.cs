﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using food_delivery.Models;

namespace food_delivery.ViewModel
{
    public class CartViewModel
    {
        public List<Dish> Dishes { get; set; }
        public ApplicationUser User { get; set; }
        public Cart Cart { get; set; }
       
    }
}   
    